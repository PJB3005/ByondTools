'''
Created on Nov 6, 2013

@author: Rob
'''
# import logging
AREA_LAYER = 1
TURF_LAYER = 2
OBJ_LAYER = 3
MOB_LAYER = 4
FLY_LAYER = 5

# @formatter:off
COLORS = {
    'aqua':    '#00FFFF',
    'black':   '#000000',
    'blue':    '#0000FF',
    'brown':   '#A52A2A',
    'cyan':    '#00FFFF',
    'fuchsia': '#FF00FF',
    'gold':    '#FFD700',
    'gray':    '#808080',
    'green':   '#008000',
    'grey':    '#808080',
    'lime':    '#00FF00',
    'magenta': '#FF00FF',
    'maroon':  '#800000',
    'navy':    '#000080',
    'olive':   '#808000',
    'purple':  '#800080',
    'red':     '#FF0000',
    'silver':  '#C0C0C0',
    'teal':    '#008080',
    'white':   '#FFFFFF',
    'yellow':  '#FFFF00'
}
# @formatter:on

_COLOR_LOOKUP = {}

def BYOND2RGBA(colorstring, alpha=255):
    colorstring = colorstring.strip()
    if colorstring == None or colorstring == '':
	    return (255, 255, 255, 255) # Edge cases of BYOND2RGBA being passed an incorrect value
    if colorstring[0] == '#':
        colorstring = colorstring[1:]
        r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
        r, g, b = [int(n, 16) for n in (r, g, b)]
        return (r, g, b, alpha)
    elif colorstring.startswith('rgb('):
        r, g, b = [int(n.strip()) for n in colorstring[4:-1].split(',')]
        return (r, g, b, alpha)
    else:
        return _COLOR_LOOKUP[colorstring]

for name, color in COLORS.items():
    _COLOR_LOOKUP[name] = BYOND2RGBA(color)

import re, hashlib, collections
from .utils import eval_expr
REGEX_TABS = re.compile('^(?P<tabs>\t*)')
class BYONDProperty(object):
    """
    Base class for BYOND variables and procs.
    """
    def __init__(self, value, filename='', line=0, typepath='/', **kwargs):
        #: The actual value.
        self.value = value

        #: Filename this was found in
        self.filename = filename

        #: Line of the originating file.
        self.line = line

        #: Typepath of the value.
        self.type = typepath

        #: Has this value been inherited?
        self.inherited = kwargs.get('inherited', False)

        #: Is this a declaration? (/var)
        self.declaration = kwargs.get('declaration', False)

    def __str__(self):
        if self.value is None:
            return 'null'
        return '{0}'.format(self.value)

    def __repr__(self):
        return '<BYONDValue value="{}" filename="{}" line={}>'.format(self.value, self.filename, self.line)

class BYONDFileRef(object):
    """
    Just to format file references differently.
    """
    def __init__(self, filename):
        self.filename = filename

    def copy(self):
        return BYONDFileRef(self.value, self.filename, self.line, declaration=self.declaration, inherited=self.inherited, special=self.special)

    def __str__(self):
        return self.filename

    def __repr__(self):
        return '<BYONDFileRef filename="{}">'.format(self.filename)

class Atom:
    '''
    An atom is, in simple terms, what BYOND considers a class.

    :param string path:
        The absolute path of this atom.  ex: */obj/item/weapon/gun*
    :param string filename:
        The file this atom originated from.
    :param int line:
        The line in the aforementioned file.
    '''

    def __init__(self, path, filename='', line=0, **kwargs):
        #: Absolute path of this atom
        self.path = path

        #: Vars of this atom, including inherited vars.
        self.properties = collections.OrderedDict()

        #: Child atoms.
        self.children = {}

        #: The parent of this atom.
        self.parent = None

        #: The file this atom originated from.
        self.filename = filename

        #: Line from the originating file.
        self.line = line

    def copy(self, toNewMap=False):
        '''
        Make a copy of this atom, without dangling references.

        :returns byond.basetypes.Atom
        '''
        new_node = Atom(self.path, self.filename, self.line, missing=self.missing)
        new_node.properties = self.properties.copy()
        new_node.mapSpecified = self.mapSpecified
        if not toNewMap:
            new_node.ID = self.ID
            new_node.old_id = self.old_id
        new_node.UpdateHash()
        # new_node.parent = self.parent
        return new_node

    def getProperty(self, index, default=None):
        '''
        Get the value of the specified property.

        :param string index:
            The name of the var we want.
        :param mixed default:
            Default value, if the var cannot be found.
        :returns:
            The desired value.
        '''
        prop = self.properties.get(index, None)
        if prop == None:
            return default
        elif prop == 'null':
            return None
        return prop.value

    def setProperty(self, index, value, flags=0):
        '''
        Set the value of a property.

        In the event the property cannot be found, a new property is added.

        This function will attempt to convert python types to BYOND types.

        :param string index:
            The name of the var desired.
        :param mixed value:
            The new value.
        :returns:
            The desired value.
        '''

        self.properties[index] = BYONDValue(value)

    def __ne__(self, atom):
        return not self.__eq__(atom)

    def __eq__(self, atom):
        if atom == None:
            return False
        # if self.mapSpecified != atom.mapSpecified:
        #    return False
        if self.path != atom.path:
            return False
        return self.properties == atom.properties


    def __lt__(self, other):
        if 'layer' not in self.properties or 'layer' not in other.properties:
            return False
        myLayer = 0
        otherLayer = 0
        try:
            myLayer = self.handle_math(self.getProperty('layer', myLayer))
        except ValueError:
            print('Failed to parse {0} as float.'.format(self.properties['layer'].value))
            pass
        try:
            otherLayer = self.handle_math(other.getProperty('layer', otherLayer))
        except ValueError:
            print('Failed to parse {0} as float.'.format(other.properties['layer'].value))
            pass
        return myLayer > otherLayer

    def __gt__(self, other):
        if 'layer' not in self.properties or 'layer' not in other.properties:
            return False
        myLayer = 0
        otherLayer = 0
        try:
            myLayer = self.handle_math(self.getProperty('layer', myLayer))
        except ValueError:
            print('Failed to parse {0} as float.'.format(self.properties['layer'].value))
            pass
        try:
            otherLayer = self.handle_math(other.getProperty('layer', otherLayer))
        except ValueError:
            print('Failed to parse {0} as float.'.format(other.properties['layer'].value))
            pass
        return myLayer < otherLayer

    def __str__(self):
        return self.serialize()

    def serialize(self, propkeys=None):
        atomContents = []
        for key, val in self.properties.items():
            atomContents += ['{0}={1}'.format(key, val)]
        return '{}{{{}}}'.format(self.path, ';'.join(atomContents))

    def dumpPropInfo(self, name):
        o = '{0}: '.format(name)
        if name not in self.properties:
            return o + 'None'
        return o + repr(self.properties[name])

"""
class Proc(Atom):
    def __init__(self, path, arguments, filename='', line=0):
        Atom.__init__(self, path, filename, line)
        self.name = self.figureOutName(self.path)
        self.arguments = arguments
        self.code = []  # (indent, line)
        self.definition = False
        self.origpath = ''

    def figureOutName(self, path):
        name = path.split('(')[0]
        return name.split('/')[-1]

    def CountTabs(self, line):
        m = REGEX_TABS.match(line)
        if m is not None:
            return len(m.group('tabs'))
        return 0

    def AddCode(self, indentLevel, line):
        self.code.append( (indentLevel, line) )

    def ClearCode(self):
        self.code = []

    def AddBlankLine(self):
        if len(self.code) > 0 and self.code[-1][1] == '':
            return
        self.code += [(0, '')]

    def MapSerialize(self, flags=0):
        return None

    def InheritProperties(self):
        return
    def getMinimumIndent(self):
        # Find minimum indent level
        for i in range(len(self.code)):
            indent, _ = self.code[i]
            if indent == 0: continue
            return indent
        return 0

    def _DumpCode(self):
        args = self.path[self.path.index('('):]
        true_path = self.path[:self.path.index('(')].split('/')
        name = true_path[-1]
        true_path = true_path[:-1]
        if self.definition:
            true_path += ['proc']
        true_path += [name + args]
        o = '\n' + '/'.join(true_path) + '\n'
        min_indent = self.getMinimumIndent()
        # Should be 1, so find the difference.
        indent_delta = 1 - min_indent
        # o += '\t// true_path  = {0}\n'.format(repr(true_path))
        # o += '\t// name       = {0}\n'.format(name)
        # o += '\t// args       = {0}\n'.format(args)
        # o += '\t// definition = {0}\n'.format(self.definition)
        # o += '\t// path       = {0}\n'.format(self.path[:self.path.index('(')])
        # o += '\t// origpath   = {0}\n'.format(self.origpath)
        # o += '\t// min_indent = {0}\n'.format(min_indent)
        # o += '\t// indent_delta = {0}\n'.format(indent_delta)
        for i in range(len(self.code)):
            indent, code = self.code[i]
            indent = max(1, indent + indent_delta)
            if code == '' and i == len(self.code) - 1:
                continue
            if code.strip() == '':
                o += '\n'
            else:
                o += (indent * '\t') + code.strip() + '\n'
        return o
"""
