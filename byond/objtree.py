'''
Superficially generate an object/property tree.
'''
import re, logging, os
import sre_constants
import xml.etree.ElementTree as ET
from byond.utils import eval_expr

try:
    import cPickle as pickle
except:
    import pickle

from .basetypes import Atom, BYONDProperty, BYONDFileRef
from .utils import md5sum, get_stdlib, CompileFile

# Valid XML tags that are classes.
DMTypes = ['object', 'area', 'turf', 'obj', 'mob']

REGEX_TABS = re.compile('^(?P<tabs>[\t\s]*)')
REGEX_ATOMDEF = re.compile('^(?P<tabs>\t*)(?P<atom>[a-zA-Z0-9_/]+)\\{?\\s*$')
REGEX_ABSOLUTE_PROCDEF = re.compile('^(?P<tabs>\t*)(?P<atom>[a-zA-Z0-9_/]+)/(?P<proc>[a-zA-Z0-9_]+)\((?P<args>.*)\)\\{?\s*$')
REGEX_RELATIVE_PROCDEF = re.compile('^(?P<tabs>\t*)(?P<proc>[a-zA-Z0-9_]+)\((?P<args>.*)\)\\{?\\s*$')
REGEX_LINE_COMMENT = re.compile('//.*?$')

def debug(filename, line, path, message):
    print('{0}:{1}: {2} - {3}'.format(filename, line, '/'.join(path), message))

class OTRCache:
    #: Only used for obliterating outdated data.
    VERSION = [27, 9, 2015]

    def __init__(self, filename):
        self.filename = filename
        self.files = {}
        self.atoms = None
        self.handle = None
        self.log = logging.getLogger(self.__class__.__name__)

    def StartReading(self):
        if self.handle:
            self.handle.close()
        self.handle = None
        if os.path.isfile(self.filename):
            self.handle = open(self.filename, 'r')

    def StopReading(self):
        if self.handle is not None:
            self.handle.close()

    def CheckVersion(self):
        # print('READ VERSION')
        # Block 1: Version
        if pickle.load(self.handle) != self.VERSION:
            self.log.warn('!!! Outdated OTR data, rebuilding.')
            return False
        return True

    def ReadFiles(self):
        # print('READ FILES')
        # Block 2: Files
        self.files = pickle.load(self.handle)

    def ReadAtoms(self):
        # print('READ ATOMS')
        return pickle.load(self.handle)

    def CheckFileHash(self, fn, md5):
        # print('{0}: {1}'.format(fn,md5))
        if fn not in self.files:
            self.log.info(' + {0}'.format(fn))
            return False
        if self.files[fn] != md5:
            self.log.info(' * {0}'.format(fn))
            return False
        return True

    def PruneFiles(self, file_list):
        for fn in self.files.keys():
            if fn not in file_list:
                self.files -= [fn]
                self.log.info(' - {0}'.format(fn))

    def SetFileMD5(self, fn, md5):
        self.files[fn] = md5

    def GetFiles(self):
        return self.files.keys()

    def Save(self, atoms):
        with open(self.filename, 'w') as f:
            pickle.dump(self.VERSION, f)
            pickle.dump(self.files, f)
            pickle.dump(atoms, f)

class ObjectTree:
    def __init__(self, **options):
        # : All atoms, in a dictionary.
        self.Atoms = {}

        # : All atoms, in a tree-node structure.
        self.Tree = Atom('')

        # : Skip loading from .OTR?
        self.skip_otr = True

        self.log = logging.getLogger(self.__class__.__name__)

    def ParseFile(self, filename):
        xml = CompileFile(filename)
        self.ParseXml(xml)

    def ParseXmlFile(self, filename):
        xml = ""
        with open(filename, "r") as f:
            xml = f.read()

        self.ParseXml(xml)

    def ParseXml(self, Xml):
        root = ET.fromstring(Xml)
        for element in root:
            if element.tag in DMTypes:
                self.ParseElement(element)

    # Recursively parse the element tree.
    def ParseElement(self, element, path = ''):
        path = '%s/%s' % (path, element.text.strip())
        if path not in self.Atoms:
            file_name = element.get('file').split(':') # file attribute is formatted like foo.dm:10.
            new_atom = Atom(path, file_name[0], file_name[1])
            self.Atoms[path] = new_atom

            # Now to add the Atom into the actual Atom tree.
            parent_path = '/'.join(path.split('/')[:-1])
            parent = self.GetAtom(parent_path) or self.Tree
            parent.children[path.split('/')[-1]] = new_atom

        for child in element:
            if child.tag in DMTypes:
                self.ParseElement(child, path)

    def SplitPath(self, string):
        o = []
        buf = []
        inProc = False
        for chunk in string.split('/'):
            if not inProc:
                if '(' in chunk and ')' not in chunk:
                    inProc = True
                    buf += [chunk]
                else:
                    o += [chunk]
            else:
                if ')' in chunk:
                    o += ['/'.join(buf + [chunk])]
                    inProc = False
                else:
                    buf += [chunk]
        return o

    def MakeTree(self):
        self.log.info('Generating Tree...')
        self.Tree = Atom('/')
        with open('objtree.txt', 'w') as f:
            for key in sorted(self.Atoms):
                f.write("{0}\n".format(key))
                atom = self.Atoms[key]
                cpath = []
                cNode = self.Tree
                fullpath = self.SplitPath(atom.path)
                truncatedPath = fullpath[1:]
                for path_item in truncatedPath:
                    cpath += [path_item]
                    cpath_str = '/'.join([''] + cpath)
                    # if path_item == 'var':
                    #    if path_item not in cNode.properties:
                    #        cNode.properties[fullpath[-1]]='???'
                    if path_item not in cNode.children:
                        if cpath_str in self.Atoms:
                            cNode.children[path_item] = self.Atoms[cpath_str]
                        else:
                            if '(' in path_item:
                                cNode.children[path_item] = Proc('/'.join([''] + cpath), [])
                            else:
                                cNode.children[path_item] = Atom('/'.join([''] + cpath), atom.filename, atom.line)
                        cNode.children[path_item].parent = cNode
                        parent_type = cNode.children[path_item].getProperty('parent_type')
                        if parent_type is not None:
                            self.log.info(' - Parent of {0} forced to be {1}'.format(cNode.children[path_item].path, repr(parent_type)))
                            cNode.children[path_item].parent = self.Atoms[parent_type]
                    cNode = cNode.children[path_item]
        self.Tree.InheritProperties()
        self.log.info('Processed {0} atoms.'.format(len(self.Atoms)))
        # self.Atoms = {}

    def GetAtom(self, path):
        if path in self.Atoms:
            return self.Atoms[path]

        cpath = []
        cNode = self.Tree
        fullpath = path.split('/')
        truncatedPath = fullpath[1:]
        for path_item in truncatedPath:
            cpath += [path_item]
            if path_item not in cNode.children:
                self.log.debug('!!! Unable to find {0} (lost at {1})'.format(path, cNode.path))
                self.log.debug(' Valid children: {0}'.format(', '.join(cNode.children.keys())))
                return None
            cNode = cNode.children[path_item]
        # print('Found {0}!'.format(path))
        self.Atoms[path] = cNode
        return cNode
